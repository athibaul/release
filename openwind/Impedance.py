import numpy as np
import warnings
try:
	import plotly.graph_objs as go
	import plotly.offline as pi
except:
	print("plotly not loading, falling back on matplotlib")
	import matplotlib.pyplot as plt
import scipy
from scipy.linalg import solve
from scipy.sparse import coo_matrix, diags, bsr_matrix, csr_matrix
from scipy.sparse.linalg import spsolve
from openwind.SpatialMatrices import SpatialMatrices
import openwind.TMMtools as tmm
from openwind.Instrument import Instrument
from openwind.Discretisation import Mesh, Nodes
from openwind.Physics import Physics

__author__ = "Juliette Chabassier, Robin Tournemenne"
__copyright__ = "Copyright 2019, Inria"
__credits__ = ["Juliette Chabassier", "Robin Tournemenne"]
__license__ = "GPL 3.0"
__version__ = "1.0"
__maintainer__ = "Juliette Chabassier"
__email__ = "juliette.chabassier@inria.fr"
__status__ = "Dev"

class Impedance:

	@staticmethod
	def l2error(z1, z2Ref):
		# for the moment it has to be calculated over the same fs!
		return np.linalg.norm(z1.imped - z2Ref.imped) / np.linalg.norm(z2Ref.imped)

	def __init__(self, *param):

		if isinstance(param[0], str):
			if param[0][-2:] == ".p":
				with open(param[0], 'rb') as fp:
					dataPickle = pickle.Unpickler(fp)
					data2copy = dataPickle.load()
					self.f = data2copy.f
					self.imped = data2copy.imped
			elif param[0][-5:] == ".json":
				with open(param[0], "r") as fp:
					imped = json.load(fp)
					self.f = imped["f"]
					self.imped = bore["imped"]
			else:
				try:
					z = np.loadtxt(param[0])
					if z.shape[1] > 3 or z.shape[1] < 2:
						raise ValueError("the txt file should have 3 columns (or 2)")
					else:
						self.f = z[:, 0]
						if z.shape[1] == 3:
							self.imped = z[:, 1] + 1j * z[:, 2]
						else:
							self.imped = z[:, 2]
				except:
					raise ValueError("Impossible to load the file")
		elif len(param) == 2:
			self.f = param[0]
			self.imped = param[1]
		# usage of the transfer matrix method
		elif len(param) == 3 or len(param) == 4:
			self.f = param[0]
			instru = param[1]
			physics = param[2]
			if len(param) == 4:
				paramSimu = param[3]
			else:
				paramSimu = {}
			self.solveTMM(self.f, instru, physics, paramSimu)
		elif len(param) == 5 or len(param) == 6:  #FEM
			self.f = param[0]
			instru = param[1]
			mesh = param[2]
			nodes = param[3]
			physics = param[4]
			if len(param) == 6:
				paramSimu = param[5]
			else:
				paramSimu = {}
			self.solveFEM(self.f, instru, mesh, nodes, physics, **paramSimu)

	def solveFEM(self, fs, instru, mesh, nodes, physics, damping=False, adim=True,
	             convention='PH1', FEMasTMM=False, nbSub=1):
		spMat = SpatialMatrices(instru, mesh, nodes, physics, adim, convention)
		omegas = 2 * np.pi * fs
		self.imped = np.empty(fs.shape, dtype=np.complex128)
		nL2 = spMat.nL2
		nH1 = spMat.nH1
		self.nL2 = nL2
		self.nH1 = nH1
		# creates alpha and beta
		instru.radiation(physics)
		if convention is 'PH1':
			# + ou -
			coefRad = (instru.alpha + 1j * omegas * instru.beta) / \
										 spMat.Zc / 1j / omegas
		elif self.convention is 'VH1':
			coefRad = spMat.Zc * 1j * omegas / (instru.alpha - 1j * omegas *
			                                    instru.beta)
		sigmah = [[nL2 + nH1 - 1], [nL2 + nH1 - 1], coefRad]
		if damping:
			instru.FEMlosses(fs, nodes, physics, FEMasTMM, nbSub)
			spMat.computeDampCoefs(instru, nodes, physics)

		massDiag = np.concatenate([spMat.massL2[1], spMat.massH1[1]])
		# tous les élément shors diagonale
		nodiag = csr_matrix((np.concatenate((-np.array(spMat.Bh[2]),
																				 np.array(spMat.Bh[2]))),
												 (spMat.Bh[0] + spMat.Bh[1], spMat.Bh[1] +
													spMat.Bh[0])), shape=(nL2 + nH1, nL2 + nH1),
												dtype='complex128')
		# print(nodiag.shape)
		# + ou -
		Lh = coo_matrix(([1], ([nL2], [0])), shape=(nL2 + nH1, 1))

		# # on crée toutes les matrices csr et on multiplie par i omega et sigma
		# # dans la boucle. méga long l'addition et la mutiplication
		# diagSp = diags(massDiag, format='csr')
		# sigma = diags(np.concatenate((np.zeros(nL2), spMat.sigma)), format='csr')
		# # on crée une seule matrice diagonale sparse mise à jour à chaque passage
		# # dans la boucle  et on fait une addition dans la boucle - trop long
		# idxDiag = np.arange(0, nL2 + nH1, 1)
		# idxPtr = np.arange(0, nL2 + nH1 + 1, 1)
		# # on ne crée qu'une CSR que dont on met à jour la diagonale - meilleure
		# # méthode
		Ah = nodiag
		diagsOmega = massDiag[..., None] * (1j * omegas)
		diagsOmega[-1, :] += sigmah[2]
		idxTrue = np.diag([True] * (nL2 + nH1))
		self.Uh = []
		with warnings.catch_warnings():
			warnings.simplefilter("ignore")
			for cpt, omega in enumerate(omegas):

				if damping:
					# pdb.set_trace()
					spMat.computeDampMat(cpt)
					nassDiag = np.concatenate([spMat.nassL2, spMat.nassH1]) * (1j * omega)
					Ah[idxTrue] = diagsOmega[:, cpt] + nassDiag
				else:
					Ah[idxTrue] = diagsOmega[:, cpt]
				Uh = spsolve(Ah, Lh, permc_spec='COLAMD')
				self.Uh.append(Uh)
				self.imped[cpt] = spMat.coefAdim * Uh[nL2]

	def solveTMM(self, f, instru, physics, paramSimu):
		for idx, part in enumerate(instru.parts):
			lpart = part[1]
			R0 = part[3][0]
			R1 = part[3][1]
			params = part[4]
			omegas = 2 * np.pi * f
			ks = omegas / physics.c
			if part[0] == 'linear':
				matrix = tmm.Cone(physics, instru, lpart, R0, R1, [omegas, ks], **paramSimu)
			else:
				raise ValueError("The part type is not implemented yet for the TMM")
			if idx == 0:
				mguide = matrix
			else:
				mguide = tmm.multmat(mguide, matrix)
		instru.radiation(physics)
		Zcend = physics.rho * physics.c / (np.pi * instru.r[-1] ** 2)
		# pdb.set_trace()
		Zr = 1j * omegas / (instru.alpha + 1j * omegas * instru.beta) * Zcend
		self.imped = tmm.impedance(mguide, Zr)
		if 'adim' in paramSimu and paramSimu['adim']:
			Zcinit = physics.rho * physics.c / (np.pi * instru.r[0] ** 2)
			self.imped = self.imped / Zcinit

	def draw(self, options=dict()):
		try:
			# dictionnary with the data and the options
			param = {}
			data = {'x': self.f, 'y': abs(self.imped)}
			param.update(data)
			param.update(options)
			return go.Scatter(param)
		except:
			if len(options) > 0:
				color = options[0]
			else:
				color = "b"
			plt.plot(self.f, abs(self.imped), "-" + color)
