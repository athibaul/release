"""This class defines the instrument.

The instrument is defined as the evolution of a radius along its
axis. It will/is also possible to define toneholes.
"""

import matplotlib.pyplot as plt
import numpy as np
import json
import pickle
import scipy.special as sp
import scipy.integrate as integrate

__author__ = "Juliette Chabassier, Robin Tournemenne"
__copyright__ = "Copyright 2019, Inria"
__credits__ = ["Juliette Chabassier", "Robin Tournemenne"]
__license__ = "GPL 3.0"
__version__ = "1.0"
__maintainer__ = "Juliette Chabassier"
__email__ = "juliette.chabassier@inria.fr"
__status__ = "Dev"

class Instrument:
	"""The actual Instrument class.

	attributes:
	-parts : list detailing every component of the bore. (also the toneholes????)
	-x : axial position at every part borders'
	-r : bore radius at every part borders'
	-s : bore surface (axysimmetric) at every part borders'
	-ltot : total length of the bore
	-xDrawing (optional) : list of lists (every part) of axial position for
	drawings
	-rDrawing (optional) : list of lists (every part) of radii for drawings

	public methods:
	-constructor
	-setNewPart : add 1 part at the end of the bore
	-delParts : delete some parts of the bore
	-setParts : define some parts of the bore
	-parts2drawing : creates xDrawing and rDrawing
  -draw : draw the bore
	-save : save a representation of the instance in a json file (or pickle file)
	-isvalid : provide some tests to know if the bore is well-defined

	"""

	acceptedBoreTypes = ["linear", "bessel"]
	acceptedBoreFeatures = ["tonehole"]

	def __init__(self, *param):
		"""Define the instrument.

    4 possibilities:
		-provide a simple bore containing the radius along the bore axis with x and
		r (these points are linearly connected)
		-same but add parts to create a more complex geometry
		-provide parts defining the instrument
		-provide a json file following one of the three methods described above, or
		directly an Instrument instance

		Parts definition:
		In this list every part is defined as a list containing 4 pieces of
		information:
		-the type of the part, e.g. linear, bessel...
		-its length or the axial position of its beginning and end
		-the radii of the beginning and the end of the part
		-the parameters used to define the shape

		For a linear part you can define only the length and radii:
		[...#previous parts#... ,
		[[x0, x1], [r0, r1]],
		...#next parts#...]

		When you define a length, the axial position of the borders of the part are
		defined according to the previous part.

		"""
		# if we load a json file
		if isinstance(param[0], str):
			if param[0][-2:] == ".p":
				with open(param[0], 'rb') as fp:
					dataPickle = pickle.Unpickler(fp)
					data2copy = dataPickle.load()
					self.x = data2copy.x
					self.r = data2copy.r
					self.s = data2copy.s
					self.parts = data2copy.parts
					self.ltot = data2copy.ltot
			elif param[0][-5:] == ".json":
				with open(param[0], "r") as fp:
					bore = json.load(fp)
					if "ltot" in bore:
						self.x = bore["x"]
						self.r = bore["r"]
						self.parts = bore["parts"]
						self.ltot = bore["ltot"]
					else:
						if "x" in bore:
							self.__init__(bore["x"], bore["r"])
							if "parts" in bore:
								self.__init__(bore["x"], bore["r"], bore["parts"])
						else:
							self.__init__(bore["parts"])
			elif param[0][-4:] == ".txt":
				xr = np.loadtxt(param[0])
				if xr.shape[1] != 2:
					raise ValueError("your .txt file needs to have 2 columns")
				self.__init__(xr[:, 0], xr[:, 1])
			else:
				raise ValueError("You have to provide the file extension which should be"
				                 " json or p (pickle file)")
		# we provide a regular bore and maybe parts
		if len(param) > 1:
			self.x = np.array(param[0])
			self.r = np.array(param[1])
			partsNb = (np.diff(self.x) > 0).astype(int)
			partsNb[partsNb != 0] = partsNb[partsNb != 0].cumsum()
			partsIdx = partsNb.tolist()

			if len(param) < 3:
				self.__bore2parts(partsIdx)
			else:
				for idx, part in enumerate(param[2]):
					# first basic verification
					part2Add = self.__partFormating(idx, part)
					self.parts.append(part2Add)
				self.__indexingParts()
			self.__xrsltotDefinition()
		# we provide a list of parts, that we need to format
		else:
			if not isinstance(param[0], str):
				self.parts = []
				for idx, part in enumerate(param[0]):
					# first basic verification
					part2Add = self.__partFormating(idx, part)
					self.parts.append(part2Add)
				self.__indexingParts()
			self.__xrsltotDefinition()

	def setNewPart(self, part):
		"""Define a new part at the end of the bore.

		You only need to provide one part to use this method. Created only for
		convenience since setParts can realize the same task
		"""
		part2Add = self.__partFormating(len(self.parts) - 1, part)
		self.parts.append(part2Add + [len(self.parts)])
		self.__xrsltotDefinition()

	def delParts(self, idxs):
		"""Delete part(s) of the bore.

		you provide an integer or a list corresponding to the number of the part you
		want to delete starting from the beginning
		"""
		if isinstance(idxs, int):
			del self.parts[idxs - 1]
		elif isinstance(idxs, list) or isinstance(idxs, np.ndarray):
			idxs.sort(reverse=True)
			for idx in idxs:
				del self.parts[idx - 1]
		else:
			raise ValueError("The expected classes for the only paramter are int, "
											 "list, or np.array")
		self.__xrsltotDefinition()

	def setParts(self, parts):
		"""Add some parts to the bore.

		You need to provide a list of parts. You should never define the length of
		the part because, if so, it will always add a part at the end of the bore.
		"""
		for idxInfo, part in enumerate(parts):
			# first basic verification
			part2Add = self.__partFormating(idxInfo, part)
			self.parts.append(part2Add + ["x"])
		self.__indexingParts()
		self.__xrsltotDefinition()

	def isvalid(self):
		"""Test if the bore is viable.

		Test if the bore begins at position 0. Test if there is no "hole" or
		superposed parts in the bore.
		"""
		# testing that the bore begins at position 0
		if self.parts[0][2][0] != 0:
			raise ValueError("The bore has to start at x = 0")
		# testing that every part is following each other
		if np.diff(np.array(self.x))[1::2].sum() != 0:
			raise ValueError("The parts doesn't follow each other, it should be a"
			                 " visually obvious problem")

	def parts2drawing(self, nbOfPoint2Disp=1000):
		"""Compute xDrawing and rDrawing.

		This let you compute two lists of ndarray you can further use to draw the
		bore. Used by the draw method or by people who would like to implement their
		own drawing strategy.
		"""
		self.xDrawing = []
		self.rDrawing = []
		for part in self.parts:
			xs = np.linspace(part[2][0], part[2][1],
			                 max([2, int(np.ceil(part[1] / self.ltot *
			                                     nbOfPoint2Disp))]))
			self.xDrawing.append(xs)
			if part[0] == "linear":
				self.rDrawing.append((part[3][0] + (part[3][1] - part[3][0]) *
															(xs - part[2][0]) / (part[2][1] - part[2][0])))
			elif part[0] == "bessel":
				d = part[1]
				gamma = part[4]
				R0 = part[3][0]
				R1 = part[3][1]
				b = (d / (R0 ** (-1 / gamma) - R1 ** (-1 / gamma))) ** gamma
				x0 = d * R1 ** (-1 / gamma) / (R0 ** (-1 / gamma) - R1 ** (-1 / gamma))
				xChVar = part[2][1] + x0 - xs
				self.rDrawing.append(b * xChVar ** (- gamma))
			elif part[0] == "exponential":
				pass
			else:
				raise ValueError("The type {} doesn't exist yet".format(part[0]))

	def draw(self, symmetric="yes"):
		"""Draw the geometry.

		Use x, r, xDrawing and rDrawing.
		"""
		self.__xrsltotDefinition()
		self.parts2drawing()
		"""Draw the bore."""
		plt.figure()
		toplot = [item.tolist() if isinstance(item, np.ndarray) else item for
		          subgroup in zip(self.xDrawing, self.rDrawing,
		                          ["b"] * len(self.xDrawing)) for item in subgroup]
		plt.plot(*toplot)
		plt.plot(self.x, self.r, 'ob')
		if symmetric == "yes":
			rDrawingSym = [- rs for rs in self.rDrawing]
			toplotsym = [item.tolist() if isinstance(item, np.ndarray) else item for
			          subgroup in zip(self.xDrawing, rDrawingSym,
			                          ["b"] * len(self.xDrawing)) for item in subgroup]
			plt.plot(*toplotsym)
			plt.plot(self.x, - np.array(self.r), 'ob')
			plt.plot(self.x, np.zeros(shape=(len(self.x),)),
			         '--k', linewidth=0.5)
		plt.show()

	def save(self, dataPath, method):
		"""Save the geometry.

		You can choose either pickle or json format
		"""
		if method == "json":
			with open(dataPath + "/instru.json", "w") as fp:
				json.dump({"parts": self.parts, "x": self.x, "r": self.r, "s":
				           self.s.tolist(), "ltot": self.ltot}, fp)
		elif method == "pickle":
			with open(dataPath + "/instru.p", "wb") as fp:
				myPck = pickle.Pickler(fp)
				myPck.dump(self)
		else:
			raise ValueError("the saving method you ask for is not understood")

	def zc(self, physics, pos):
		if callable(physics.c):
			return physics.rho(self.x[pos]) * physics.c(self.x[pos]) / self.s[pos]
		else:
			return physics.rho * physics.c / self.s[pos]

	def radiation(self, physics, category="rabiner"):
		if callable(physics.c):
			c = physics.c(self.r[-1])
		else:
			c = physics.c
		if category is "rabiner":
			self.alpha = 3 * c * np.pi / 8 / self.r[-1]
			self.beta = 9 * np.pi ** 2 / 128
		elif category is "pipe":
			deltaL = 0.6
			alpha_chaigne = 1 / 4
			self.alpha = c / deltaL / self.r[-1]
			#self.alpha = c / deltaL / self.x[-1]
			self.beta = alpha_chaigne / deltaL ** 2

	def FEMlosses(self, fs, nodes, physics, FEMasTMM, nbSub):
		# asTMM : bool, false if regular equation, true if TMM equation

		#3D method + imaginary incorporation before dissipTerms
		sqrtFs = np.sqrt(fs)
		if callable(physics.c):
			if hasattr(nodes, 'rs'):  # if adaptative
				self.rv = []
				self.rt = []
				for rLoc, xLoc in nodes.rLoc, nodes.xNodes:
					if len(rLoc) > 0:
						self.rv.append(rLoc[..., None] *
						               (np.sqrt(2 * np.pi / (physics.lv(xLoc)[..., None])) *
						                sqrtFs))
						self.rt.append(rLoc[..., None] *
						               (np.sqrt(2 * np.pi / (physics.lt(xLoc)[..., None])) *
						                sqrtFs))
					else:
						self.rv.append([])
						self.rt.append([])
			else:
				self.rv = nodes.rLoc[..., None] * \
					(np.sqrt(2 * np.pi / (physics.lv(nodes.xNodes)[..., None])) * sqrtFs)
				self.rt = nodes.rLoc[..., None] * \
					(np.sqrt(2 * np.pi / (physics.lt(nodes.xNodes)[..., None])) * sqrtFs)
		else:
			if hasattr(nodes, 'rs'):  # if adaptative
				self.rv = []
				self.rt = []
				for rLoc, xLoc in zip(nodes.rLoc, nodes.xNodes):
					if len(rLoc) > 0:
						self.rv.append(rLoc[..., None] * (np.sqrt(2 * np.pi / (physics.lv)) *
																							sqrtFs))
						self.rt.append(rLoc[..., None] * (np.sqrt(2 * np.pi / (physics.lt)) *
																							sqrtFs))
					else:
						self.rv.append([])
						self.rt.append([])
			else:
				rLoc = nodes.rLoc
				if FEMasTMM:
					# print('In FEMLosses, with FEMasTMM= ' + str(FEMasTMM))
					xLoc = nodes.xNodes
					# print('First rLoc point before loop = ' + str(rLoc[0,0]))
					# print(' rLoc point before loop = ' +
					# pdb.set_trace()
					# print(rLoc[0,:])
					# print('Last rLoc point before loop = ' + str(rLoc[0,-1]))
					rLoc = np.zeros(rLoc.shape)
					for ielt, xelt in enumerate(xLoc.transpose()):
						rtmp = self.x2r(xelt.mean(), nbSub)
						rLoc[:,ielt] = rtmp					
					# print('First rLoc point after loop = ' + str(rLoc[0,0]))
					# print('Last rLoc point after loop = ' + str(rLoc[0,-1]))

				self.rv = rLoc[..., None] * (np.sqrt(2 * np.pi / (physics.lv)) * sqrtFs)
				self.rt = rLoc[..., None] * (np.sqrt(2 * np.pi / (physics.lt)) * sqrtFs)
		self.__dissipTerms()

	def TMMlosses(self, omegas, Rmoy, physics):
		# print('In TMMLosses, ')
		# print('Rmoy value = ' + str(Rmoy))
		self.rv = Rmoy * np.sqrt(physics.rho * omegas / physics.mu)
		nu = np.sqrt(physics.mu * physics.Cp / physics.kappa)
		# pdb.set_trace()
		self.rt = self.rv * nu
		self.__dissipTerms()

	def losses(self, f, r, physics):
		sqrtFs = np.sqrt(f)
		# pdb.set_trace()
		rv = r * (np.sqrt(2 * np.pi / (physics.lv)) * sqrtFs)
		rt = r * (np.sqrt(2 * np.pi / (physics.lt)) * sqrtFs)
		coef = np.sqrt(1j)
		factkv = 2 * coef * sp.jve(1, rv / coef) / \
			(rv * sp.jve(0, rv / coef))
		# factkv = 2 * coef * sp.jve(1, self.rv) / (self.rv * sp.jve(0, self.rv))
		coefZ = factkv / (1 - factkv)
		coefY = 2 * coef * sp.jve(1, rt / coef) / \
			(rt * sp.jve(0, rt / coef))
		return coefZ, coefY

	def zv(self, r, f, physics):
		omega = 2 * np.pi * f
		# sqrt of 1j is ambiguous... according to the implementation, it can lead to
    # different results! In our case, the best was to use 1/sqrt(1j)
		kv = np.sqrt(omega * physics.rho / physics.mu)
		jv = 2 * sp.jve(1, kv * r) / (kv * r * sp.jve(0, kv * r))
		return 1j * omega * physics.rho / (np.pi * r**2) / (1 - jv)

	def yt(self, r, f, physics):
		omega = 2 * np.pi * f
		# sqrt of 1j is ambiguous... according to the implementation, it can lead to
    # different results! In our case, the best was to use 1/sqrt(1j)
		kt = np.sqrt(omega * physics.rho * physics.Cp / physics.kappa) / np.sqrt(1j)
		jt = 2 * sp.jve(1, kt * r) / (kt * r * sp.jve(0, kt * r))
		return 1j * omega * (np.pi * r**2) / (physics.rho * physics.c**2) * \
			(1 + (physics.gamma - 1) * jt)

	def lambdaCyl(self, r, f, physics):
		omega = 2 * np.pi * f
		# sqrt of 1j is ambiguous... according to the implementation, it can lead to
		# different results! In our case, the best was to use 1/sqrt(1j)
		kv = np.sqrt(omega * physics.rho / physics.mu) / np.sqrt(1j)
		jv = 2 * sp.jve(1, kv * r) / (kv * r * sp.jve(0, kv * r))
		kt = np.sqrt(omega * physics.rho * physics.Cp / physics.kappa) / np.sqrt(1j)
		jt = 2 * sp.jve(1, kt * r) / (kt * r * sp.jve(0, kt * r))
		coef = np.sqrt((1 - jv) / (1 + (physics.gamma - 1) * jt))
		return abs(physics.c / f * coef), abs(coef)

	def __dissipTerms(self):
		# equivalent to say -j in rv and rt instead of j. It is a convention because
		# the sqrt of j is not unique.
		coef = np.sqrt(1j)

		if isinstance(self.rv, list):  # if adaptative
			self.coefZ = []
			self.coefY = []
			for rv, rt in zip(self.rv, self.rt):
				if len(rv) > 0:
					factkv = 2 * coef * sp.jve(1, rv / coef) / (rv *
					                                            sp.jve(0, rv / coef))
					# factkv = 2 * coef * sp.jve(1, self.rv) / (self.rv * sp.jve(0, self.rv))
					self.coefZ.append(factkv / (1 - factkv))
					self.coefY.append(2 * coef * sp.jve(1, rt / coef) /
					                  (rt * sp.jve(0, rt / coef)))
				else:
					self.coefZ.append([])
					self.coefY.append([])
		else:
			factkv = 2 * coef * sp.jve(1, self.rv / coef) / (self.rv *
			                                                 sp.jve(0, self.rv / coef))
			# factkv = 2 * coef * sp.jve(1, self.rv) / (self.rv * sp.jve(0, self.rv))
			self.coefZ = factkv / (1 - factkv)
			self.coefY = 2 * coef * sp.jve(1, self.rt / coef) / \
				(self.rt * sp.jve(0, self.rt / coef))
			# pdb.set_trace()

	def x2r(self, xs, subdiv=1):
		# vectoriser le code!!
		if xs > self.ltot or xs < 0:
			print(xs)
			raise ValueError("x needs to be between the beginning (0) and the end " +
			                 "of the instrument (ltot)")
		else:
			for part in self.parts:
				lpart = part[1]
				x = part[2]
				r = part[3]
				if xs >= x[0] and xs <= x[1]:
					if r[0] == r[1]: # test cylindre 
						subPart = 1
					else:
						subPart = subdiv
					lcur = lpart / subPart
					for i in range(subPart):
						R0 = r[1] + (r[0] - r[1]) * (lpart - i * lcur) / lpart
						R1 = r[1] + (r[0] - r[1]) * (lpart - (i + 1) * lcur) / lpart
						X0 = x[1] + (x[0] - x[1]) * (lpart - i * lcur) / lpart
						X1 = x[1] + (x[0] - x[1]) * (lpart - (i + 1) * lcur) / lpart

						if xs >= X0 and xs <= X1:
							if part[0] == "linear":
								rs = (2 * min(R0, R1) + max(R0, R1)) / 3
							elif part[0] == "bessel":
								pass
							elif part[0] == "exponential":
								pass
							return rs

	def __bore2parts(self, partsIdx):
		if hasattr(self, "parts"):
			pass
		else:
			self.parts = []
			for idx, partIdx in enumerate(partsIdx):
				if partIdx > 0:
					self.parts.append(["linear", self.x[idx + 1] - self.x[idx],
					                   [self.x[idx], self.x[idx + 1]],
					                   [self.r[idx], self.r[idx + 1]], [], partIdx])

	def __partFormating(self, idx, part):
		if len(part) < 2:
			raise ValueError("The size of the part {} is too short"
											 .format(idx + 1))

		# management of the part's type
		if not isinstance(part[0], str):
			partFinal = self.__boreTypesFormating(part, idx, "linear")
		elif any(typ in part[0] for typ in Instrument.acceptedBoreTypes):
			partFinal = self.__boreTypesFormating(part[1:], idx, part[0])
		elif any(feat in part[0] for feat in Instrument.acceptedBoreFeatures):
			# si ce sont des éléments tels que des trous.
			pass
		else:
			raise ValueError("the requested type for the part {} is not "
											 "currently supported".format(idx + 1))

		return partFinal

	def __boreTypesFormating(self, part, idx, boreType):
		partFinal = [boreType]
		# management of the axial position

		# the user gave a length and not a couple of positions:
		if isinstance(part[0], float):
			if part[0] < 0 or not (isinstance(part[0], float) or
														 isinstance(part[0], int)):
				raise ValueError("The length you entered for part {} is not "
												 " recognised".format(idx + 1))
			if len(self.parts) > 0:
				lastx = self.parts[-1][2][1]
				partFinal = partFinal + [part[0], [lastx, lastx + part[0]]]
			else:
				partFinal = partFinal + [part[0], [0, part[0]]]
		elif len(part[0]) == 2:
			if part[0][1] < part[0][0]:
				raise ValueError("The end of part {} should be farther than"
												 " its beginning along the axis! Check its x "
												 "definition".format(idx + 1))
			elif len(self.parts) == 0 and part[0][0] is not 0:
				raise ValueError("The first element should start at position 0")
			partFinal = partFinal + [part[0][1] - part[0][0], part[0]]
		else:
			raise ValueError("The length/axial positions of part {} is not"
			                 "recognised".format(idx + 1))

		# management of the radii of the border
		if len(part[1]) == 2 and \
		(isinstance(part[1][0], int) or isinstance(part[1][0], float)) and \
		(isinstance(part[1][1], int) or isinstance(part[1][1], float)):
			if part[1][0] > 0 and part[1][1] > 0:
				partFinal.append(part[1])
			else:
				raise ValueError("The radii must be positive (problem regarding "
												 "part {})".format(idx + 1))
		else:
			raise ValueError("The radii definition for the part {} is "
											 "wrong".format(idx + 1))

		# management of the type special parameters
		if len(part) == 2:
			if partFinal[0] is "linear":
				partFinal.append([])
			else:
				raise ValueError("Since the part type is not linear, we need some "
												 "type special parameters (problem part {})"
												 .format(idx + 1))
		elif len(part) == 3:
			partFinal.append(part[2])
		elif len(part) > 3:
			raise ValueError("the part {} is too long!".format(idx + 1))

		return partFinal

	def __indexingParts(self):
		# sorting partsTemp for indexing:
		self.parts.sort(key=lambda x: x[2][0])
		if len(self.parts[0]) is 5:
			self.parts = [part + [idx] for idx, part in enumerate(self.parts)]
		elif len(self.parts[0]) is 6:
			self.parts = [part[:-1] + [idx] for idx, part in enumerate(self.parts)]

	def __xrsltotDefinition(self):
		self.x = [item for part in self.parts for item in part[2]]
		self.r = [item for part in self.parts for item in part[3]]
		self.s = np.pi * np.array(self.r) ** 2
		self.ltot = self.parts[-1][2][1]

	def __str__(self):
		return "coucou c'est moi ! "
