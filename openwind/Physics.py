"""This class defines physical quantities.

Given a certain temperature temp, you will compute quantities like c, rho...
"""

import numpy as np

__author__ = "Juliette Chabassier, Robin Tournemenne"
__copyright__ = "Copyright 2019, Inria"
__credits__ = ["Juliette Chabassier", "Robin Tournemenne"]
__license__ = "GPL 3.0"
__version__ = "1.0"
__maintainer__ = "Juliette Chabassier"
__email__ = "juliette.chabassier@inria.fr"
__status__ = "Dev"

class Physics:
	"""The actual Physics class.

	attributes:
	-T: temperature in Kelvin
	-rho: air density
	-c: air sound speed
	-mu
	-Cp
	-kappa
	-gamma
	-khi
	-lt: almost a characteristic distance for loss computation
	-lv: almost a characteristic distance for loss computation

	"""

	def __init__(self, temp):
		"""Define the quantitities described above.

		You can find the formulae in the Chaigne and Kergomard book
		"""
		T0 = 273.15
		self.T0 = T0
		self.Cp = 240
		self.gamma = 1.402
		if callable(temp):
			def T(x):
				return temp(x) + T0

			def rho(x):
				return 1.2929 * T0 / T(x)

			def c(x):
				return 331.45 * np.sqrt(T(x) / T0)

			def mu(x):
				return 1.7141e-5 * np.sqrt(T0 / T(x))

			def kappa(x):
				return 5.77 * 1e-3 * (1 + 0.0033 * temp(x))

			def khi(x):
				return 1 / rho(x) / c(x) ** 2

			def lt(x):
				return kappa(x) / rho(x) / self.Cp

			def lv(x):
				return mu(x) / rho(x)

			self.T = T
			self.temp = temp
			self.rho = rho
			self.c = c
			self.mu = mu
			self.kappa = kappa
			self.khi = khi
			self.lt = lt
			self.lv = lv
		else:
			T = temp + T0
			self.T = T
			self.temp = temp
			self.rho = 1.2929 * T0 / T
			self.c = 331.45 * np.sqrt(T / T0)

			# losses coefficients
			self.mu = 1.7141e-5 * np.sqrt(T0 / T)
			self.kappa = 5.77 * 1e-3 * (1 + 0.0033 * self.temp)
			self.khi = 1 / self.rho / self.c ** 2
			# not really lt and lv because I don't want to use the velocity c for nothing
			self.lt = self.kappa / self.rho / self.Cp
			self.lv = self.mu / self.rho
