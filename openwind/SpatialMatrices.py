import numpy as np
import os
from openwind.Instrument import Instrument
from openwind.Discretisation import Mesh, Nodes
from openwind.Physics import Physics
import matplotlib.pyplot as plt

__author__ = "Juliette Chabassier, Robin Tournemenne"
__copyright__ = "Copyright 2019, Inria"
__credits__ = ["Juliette Chabassier", "Robin Tournemenne"]
__license__ = "GPL 3.0"
__version__ = "1.0"
__maintainer__ = "Juliette Chabassier"
__email__ = "juliette.chabassier@inria.fr"
__status__ = "Dev"

class SpatialMatrices:

	def __init__(self, instru, mesh, nodes, physics, adim=True, convention="PH1"):
		self.sStar = min(instru.s)
		self.nbEles = mesh.nbEles
		self.lEles = mesh.lEles
		self.adim = adim
		self.convention = convention
		if self.adim:
			self.Zc = self.sStar / instru.s[-1]
			if callable(physics.c):
				# to check! not sure it is the first position that we want
				self.coefAdim = physics.rho(instru.x[0]) * physics.c(instru.x[0]) / \
					self.sStar
			else:
				self.coefAdim = physics.rho * physics.c / self.sStar
		else:
			self.Zc = instru.zc(physics, -1)
			self.coefAdim = 1
		self.computeMatrices(nodes, physics)
		# 1D array
		self.Eh = np.zeros(self.nH1)
		self.Eh[0] = 1
		# diagonal matrix conserved in a 1D array
		self.sigma = np.zeros(self.nH1)
		self.sigma[-1] = 1

	def computeMatrices(self, nodes, physics):
		if not callable(physics.c):
			factMult = physics.rho * physics.c ** 2
		if hasattr(nodes, 'rs'):  # if adaptative
			self.nH1 = nodes.rs.sum() + 1
			self.nL2 = (nodes.rs + 1).sum()
		else:
			self.nH1 = self.nbEles * nodes.r + 1
			self.nL2 = (nodes.r + 1) * self.nbEles
		self.xH1 = np.empty(self.nH1,)
		self.xL2 = np.empty(self.nL2,)
		# two diagonal coo matrix conserved in a list of 2 lists: rows/columns and
    # data
    # I don't use anymore the coo_matrix property of summing when generating
		# self.massH1 = [[], []]
		self.massH1 = [(np.arange(0, self.nH1, 1, dtype='int') + self.nL2).tolist(),
		               np.zeros(self.nH1), []]
		self.massL2 = [np.arange(0, self.nL2, 1, dtype='int').tolist(),
		               np.zeros(self.nL2), []]
		self.Bh = [[], [], [], []]
		self.ddlH1 = []
		self.ddlL2 = []
		for k in range(self.nbEles):
			if hasattr(nodes, 'rs'):  # if adaptative
				curR = nodes.rs[k]
				if k == 0:
					ddlH1 = np.arange(0, curR + 1)
					ddlL2 = np.arange(0, curR + 1)
				else:
					ddlH1 = np.arange(ddlH1[-1], ddlH1[-1] + curR + 1)
					ddlL2 = np.arange(ddlL2[-1] + 1, ddlL2[-1] + 1 + curR + 1)
			else:
				ddlH1 = np.arange(nodes.r * k, nodes.r * (k + 1) + 1)
				ddlL2 = np.arange((nodes.r + 1) * k, (nodes.r + 1) * (k + 1))
			self.ddlH1.append(ddlH1)
			self.ddlL2.append(ddlL2)
			if hasattr(nodes, 'rs'):  # if adaptative
				sLoc = nodes.sLoc[curR - 1][:, nodes.idxOrder[k]]
				xNodes = nodes.xNodes[curR - 1][:, nodes.idxOrder[k]]
			else:
				sLoc = nodes.sLoc[:, k]
				xNodes = nodes.xNodes[:, k]
			if callable(physics.c):
				if self.adim:
					rhoSurS = self.sStar / sLoc / physics.c(xNodes)
					sSurRhoC2 = sLoc / self.sStar / physics.c(xNodes)
				else:
					factMult = physics.rho(xNodes) * physics.c(xNodes) ** 2
					rhoSurS = physics.rho(xNodes) / sLoc
					sSurRhoC2 = sLoc / factMult
			else:
				if self.adim:
					rhoSurS = self.sStar / sLoc / physics.c
					sSurRhoC2 = sLoc / self.sStar / physics.c
				else:
					rhoSurS = physics.rho / sLoc
					sSurRhoC2 = sLoc / factMult
			# pdb.set_trace()
			self.xH1[ddlH1] = xNodes
			self.xL2[ddlL2] = xNodes
			[ddlL2s, ddlH1s] = np.meshgrid(ddlL2, ddlH1)
			self.Bh[0].extend(ddlL2s.flatten()),
			self.Bh[1].extend(ddlH1s.flatten() + self.nL2)  # nL2: anticipation de Ah
			if hasattr(nodes, 'rs'):  # if adaptative
				self.Bh[2].extend(nodes.quads[curR - 1].BK.transpose().flatten())
			else:
				self.Bh[2].extend(nodes.BK.transpose().flatten())
			# for basic Bh representation (ReedInstrument)
			self.Bh[3].extend(ddlH1s.flatten())

			if self.convention is "VH1":
				factMultH1 = rhoSurS
				factMultL2 = sSurRhoC2
			elif self.convention is "PH1":
				factMultH1 = sSurRhoC2
				factMultL2 = rhoSurS

			lEle = self.lEles[k]
			# I don't use anymore the coo_matrix property of summing when generating
			self.massH1[2].extend(ddlH1 + self.nL2)  # for nass matrices
			self.massL2[2].extend(ddlL2)  # for nass matrices
			# pdb.set_trace()
			if hasattr(nodes, 'rs'):  # if adaptative
				self.massH1[1][ddlH1] += lEle * nodes.quads[curR - 1].weight * factMultH1
				self.massL2[1][ddlL2] = lEle * nodes.quads[curR - 1].weight * factMultL2
			else:
				self.massH1[1][ddlH1] += lEle * nodes.weight * factMultH1
				self.massL2[1][ddlL2] = lEle * nodes.weight * factMultL2

	def computeDampCoefs(self, instru, nodes, physics):
		if hasattr(nodes, 'rs'):  # if adaptative
			self.nodes = nodes
			nassH1items = [[]] * len(nodes.rLoc)
			nassL2items = [[]] * len(nodes.rLoc)
			# for each adaptative order
			for idx, sLoc, xLoc, lEles, coefZ, coefY in zip(range(len(nodes.sLoc)),
			                                          nodes.sLoc,
			                                          nodes.xNodes,
			                                          nodes.lElesOrder[:len(nodes.sLoc)],
			                                          instru.coefZ, instru.coefY):
				if len(sLoc) > 0:
					if callable(physics.c):
						factMult = physics.rho(xLoc) * physics.c(xLoc) ** 2
						if self.adim:
							rhoSurS = self.sStar / (sLoc * physics.c(xLoc))
							sSurRhoC2 = sLoc / (self.sStar * physics.c(xLoc))
						else:
							rhoSurS = physics.rho(xLoc) / sLoc
							sSurRhoC2 = sLoc / factMult

					else:
						factMult = physics.rho * physics.c ** 2
						if self.adim:
							rhoSurS = self.sStar / (sLoc * physics.c)
							sSurRhoC2 = sLoc / (self.sStar * physics.c)
						else:
							rhoSurS = physics.rho / sLoc
							sSurRhoC2 = sLoc / factMult

					rhoSurS = rhoSurS[..., None] * coefZ
					sSurRhoC2 = sSurRhoC2[..., None] * (physics.gamma - 1) * coefY
					if self.convention is "VH1":
						factMultH1 = rhoSurS
						factMultL2 = sSurRhoC2
					elif self.convention is "PH1":
						factMultH1 = sSurRhoC2
						factMultL2 = rhoSurS
					# J'en suis là je dois créer une variable lors de la création du
					# mesh afin d'avoir lEles de l'ordre étudié à ce moment là.
					# pdb.set_trace()
					nassH1items[idx] = lEles[None, ..., None] * \
						nodes.quads[idx].weight[..., None, None] * factMultH1
					nassL2items[idx] = lEles[None, ..., None] * \
						nodes.quads[idx].weight[..., None, None] * factMultL2
			# order-wise ordering
			# self.nassH1items = nassH1items
			# self.nassL2items = nassL2items

			# ordering element-wise for code performance optimization
			for idx, curR, idxR in zip(range(len(nodes.rs)), nodes.rs, nodes.idxOrder):
				# pdb.set_trace()
				if idx == 0:
					self.nassH1items = [nassH1items[curR - 1][:, idxR, :]]
					self.nassL2items = [nassL2items[curR - 1][:, idxR, :]]
				else:
					self.nassH1items.append(nassH1items[curR - 1][:, idxR, :])
					self.nassL2items.append(nassL2items[curR - 1][:, idxR, :])
		else:
			if callable(physics.c):
				if self.adim:
					rhoSurS = self.sStar / (nodes.sLoc * physics.c(nodes.xNodes))
					sSurRhoC2 = nodes.sLoc / (self.sStar * physics.c(nodes.xNodes))
				else:
					factMult = physics.rho(nodes.xNodes) * physics.c(nodes.xNodes) ** 2
					rhoSurS = physics.rho(nodes.xNodes) / nodes.sLoc
					sSurRhoC2 = nodes.sLoc / factMult
			else:
				factMult = physics.rho * physics.c ** 2
				if self.adim:
					rhoSurS = self.sStar / (nodes.sLoc * physics.c)
					sSurRhoC2 = nodes.sLoc / (self.sStar * physics.c)
				else:
					rhoSurS = physics.rho / nodes.sLoc
					sSurRhoC2 = nodes.sLoc / factMult
			rhoSurS = rhoSurS[..., None] * instru.coefZ
			sSurRhoC2 = sSurRhoC2[..., None] * (physics.gamma - 1) * instru.coefY
			if self.convention is "VH1":
				factMultH1 = rhoSurS
				factMultL2 = sSurRhoC2
			elif self.convention is "PH1":
				factMultH1 = sSurRhoC2
				factMultL2 = rhoSurS
			self.nassH1items = self.lEles[None, ..., None] * \
				nodes.weight[..., None, None] * factMultH1
			self.nassL2items = self.lEles[None, ..., None] * \
				nodes.weight[..., None, None] * factMultL2

	def computeDampMat(self, cpt=0):
		self.nassH1 = np.zeros(self.nH1, dtype='complex128')
		self.nassL2 = np.zeros(self.nL2, dtype='complex128')
		if hasattr(self, 'nodes'):  # adaptative mesh
			for k in range(self.nbEles):
				self.nassH1[self.ddlH1[k]] += self.nassH1items[k][:, cpt]
				self.nassL2[self.ddlL2[k]] += self.nassL2items[k][:, cpt]
			# order-wide instead of element-wise
			# rs = self.nodes.rs
			# idxRs = self.nodes.idxOrder
			# for k in range(self.nbEles):
			# 	self.nassH1[self.ddlH1[k]] += self.nassH1items[rs[k] - 1][:, idxRs[k], cpt]
			# 	self.nassL2[self.ddlL2[k]] += self.nassL2items[rs[k] - 1][:, idxRs[k], cpt]
		else:
			for k in range(self.nbEles):
				self.nassH1[self.ddlH1[k]] += self.nassH1items[:, k, cpt]
				self.nassL2[self.ddlL2[k]] += self.nassL2items[:, k, cpt]
