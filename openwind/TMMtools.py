import numpy as np
import pdb

__author__ = "Juliette Chabassier, Robin Tournemenne"
__copyright__ = "Copyright 2019, Inria"
__credits__ = ["Juliette Chabassier", "Robin Tournemenne"]
__license__ = "GPL 3.0"
__version__ = "1.0"
__maintainer__ = "Juliette Chabassier"
__email__ = "juliette.chabassier@inria.fr"
__status__ = "Dev"

def Cone(physics, instru, lpart, R0, R1, omegasKs, damping=False, sph=False, nbSub=1):
	if damping:
		omegas = omegasKs[0]
		Rbeg = R0
		Rend = R1
		if Rbeg == Rend:# test cylndrique 
			subPart = 1
		else:
			subPart = nbSub
		lcur = lpart / subPart
		for i in range(subPart):
			R0 = Rend + (Rbeg - Rend) * (lpart - i * lcur) / lpart
			R1 = Rend + (Rbeg - Rend) * (lpart - (i + 1) * lcur) / lpart

			L = np.sqrt(lcur ** 2 + (R0 - R1) ** 2)
			if sph:
				DR = R1 - R0
				beta = DR / R0 * (lcur ** 2 + DR ** 2) ** (-1 / 2)
				h = DR * R0 / ((lcur ** 2 + DR ** 2) ** (1 / 2) + lcur)
				S = np.pi * (R0 ** 2 + h ** 2)
			else:
				beta = (R1 - R0) / lcur / R0
				S = np.pi * R0 ** 2
			Zc = physics.rho * physics.c / S

			# Rmoy = (R0 + R1) / 2  # half
			Rmoy = (2 * min(R0, R1) + max(R0, R1)) / 3  # adapted to converging and diverging pipes
			# Dalmont
			# pdb.set_trace()
			instru.TMMlosses(omegas, Rmoy, physics)
			Zv = 1j * omegas * physics.rho / S * (1 + instru.coefZ)
			Yt = 1j * omegas * physics.khi * S * (1 +
			                                      (physics.gamma - 1) * instru.coefY)

			Gamma = np.sqrt(Zv * Yt)
			Zcc = np.sqrt(Zv / Yt)
			if sph:
				length = L
			else:
				length = lcur
			A = R1 / R0 * np.cosh(Gamma * length) - \
			    beta / Gamma * np.sinh(Gamma * length)
			B = R0 / R1 * Zcc * np.sinh(Gamma * length)
			C = 1 / Zcc * ((R1 / R0 - beta ** 2 / Gamma ** 2) *
			               np.sinh(Gamma * length) + length * beta ** 2 /
			               Gamma * np.cosh(Gamma * length))
			D = R0 / R1 * (np.cosh(Gamma * length) + (beta / Gamma) *
			               np.sinh(Gamma * length))

			matrixLocal = np.concatenate([A, B, C, D])
			if i == 0:
				mat = matrixLocal
			else:
				mat = multmat(mat, matrixLocal)
		return mat
	else:
		ks = omegasKs[1]
		L = np.sqrt(lpart ** 2 + (R0 - R1) ** 2)
		if sph:
			DR = R1 - R0
			beta = DR / R0 * (lpart ** 2 + DR ** 2) ** (-1 / 2)
			h = DR * R0 / ((lpart ** 2 + DR ** 2) ** (1 / 2) + lpart)
			S = np.pi * (R0 ** 2 + h ** 2)
			Zc = physics.rho * physics.c / S
			length = L
		else:
			beta = (R1 - R0) / lpart / R0
			Zc = physics.rho * physics.c / (np.pi * R0 ** 2)
			length = lpart
		A = R1 / R0 * np.cos(ks * length) - beta / ks * np.sin(ks * length)
		B = R0 / R1 * 1j * Zc * np.sin(ks * length)
		C = 1j / Zc * ((R1 / R0 + beta ** 2 / ks ** 2) * np.sin(ks * length) -
		               length * beta ** 2 / ks * np.cos(ks * length))
		D = R0 / R1 * (np.cos(ks * length) + (beta / ks) * np.sin(ks * length))
		return np.concatenate([A, B, C, D])


def multmat(mguide, matrix):
	N = int(mguide.shape[0] / 4)
	A = mguide[0:N] * matrix[0:N] + \
	    mguide[N:2 * N] * matrix[2 * N:3 * N]
	B = mguide[0:N] * matrix[N:2 * N] + \
	    mguide[N:2 * N] * matrix[3 * N:4 * N]
	C = mguide[2 * N:3 * N] * matrix[0:N] + \
	    mguide[3 * N:4 * N] * matrix[2 * N:3 * N]
	D = mguide[2 * N:3 * N] * matrix[N:2 * N] + \
	    mguide[3 * N:4 * N] * matrix[3 * N:4 * N]
	return np.concatenate([A, B, C, D])

def impedance(mguide, Zr):
	N = int(mguide.shape[0] / 4)
	return (mguide[0:N] * Zr +
	        mguide[N:2 * N]) / (mguide[2 * N:3 * N] * Zr + mguide[3 * N:4 * N])
