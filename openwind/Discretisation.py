"""Discretize the instrument along space.

this class creates the space mesh and calculate the corresponding
nodes based on a gauss-lobato quadrature.
"""
from openwind.Instrument import Instrument
from openwind.GLQuad import GLQuad
import numpy as np
import plotly.graph_objs as go
import plotly.offline as pi

#print("plotly not loading, falling back on matplotlib")
import matplotlib.pyplot as plt

import json
import pickle
import pdb

__author__ = "Juliette Chabassier, Robin Tournemenne"
__copyright__ = "Copyright 2019, Inria"
__credits__ = ["Juliette Chabassier", "Robin Tournemenne"]
__license__ = "GPL 3.0"
__version__ = "1.0"
__maintainer__ = "Juliette Chabassier"
__email__ = "juliette.chabassier@inria.fr"
__status__ = "Dev"

class Mesh:
	"""Calculate the mesh and the nodes for the quadrature given instrument."""

	def __init__(self, *param):
		"""Create the discretisation for a given Instrument."""
		if isinstance(param[0], str):
			if param[0][-2:] == ".p":
				with open(param[0], 'rb') as fp:
					dataPickle = pickle.Unpickler(fp)
					data2copy = dataPickle.load()
					self.eles = data2copy.eles
					self.mesh = data2copy.mesh
					self.lEles = data2copy.lEles
					self.nbEles = data2copy.nbEles
			elif param[0][-5:] == ".json":
				with open(param[0], "r") as fp:
					mesh = json.load(fp)
					self.eles = mesh["eles"]
					self.mesh = mesh["mesh"]
					self.lEles = np.array(mesh["lEles"])
					self.nbEles = mesh["nbEles"]
		else:
			instru = param[0]
			instru.isvalid()
			self.mesh = []
			self.eles = []
			if len(param) > 3:  # adaptative, still experimental
				self.rs = []
				self.shortestLbd = []
				dofPerLbd = param[1]
				phys = param[2]
				fMax = param[3]
				shortestLbd = []
				if len(param) == 5:
					infLim = param[4]['infLim']
					supLim = param[4]['supLim']
				else:
					infLim = 1
					supLim = 10
				self.lElesOrder = [np.array([])] * supLim
				# shortest wavelength calculation
				for part in instru.parts:
					lgt = part[1]
					x = part[2]
					r = part[3]
					if part[0] == "linear":
						# min and abs in order to treat convergent and divergent cones
						xConeInv = np.abs(r[1] - r[0]) / (lgt * np.min(r))
						highestK = xConeInv + np.sqrt(xConeInv ** 2 + (2 * np.pi * fMax / phys.c) ** 2)
						shortestLbd.append((2 * np.pi / highestK))
						omega = 2 * np.pi * fMax
						# je pense que c'est faux: dans un cone la dérivée du rayon par
						# rapport à l'axe est constante mais la dérivée de la surface le
						# long de l'axe varie. donc on ne peut pas calculer en dur dSdxOverS
						# dSdxOverS = np.abs(r[1]**2 - r[0]**2) / (lgt * min(r)**2)
						# Delta = 4 * omega**2 / phys.c**2 + dSdxOverS**2
						# highestK = 0.5 * (dSdxOverS + np.sqrt(Delta))
						# self.lbdworst.append(2 * np.pi / kworst)
						# shortestLbd.append(2 * np.pi / highestK)

				# empirical method to adapt the value of the shortest wavelength in
				# order to get descent orders for small elemnents. non working
				# unfortunately.

				# self.shortestLbd.append(min(shortestLbd[0], shortestLbd[1]))
				# for idx,part in enumerate(instru.parts):
				# 	if(idx != len(instru.parts)-1 and idx !=0):
				# 		self.shortestLbd.append(min([shortestLbd[idx-1], shortestLbd[idx], shortestLbd[idx+1]]))
				# self.shortestLbd.append(min(shortestLbd[-2], shortestLbd[-1]))
				self.shortestLbd = shortestLbd

				for idx, part in enumerate(instru.parts):
					lgt = part[1]
					x = part[2]
					r = part[3]
					if part[0] == "linear":
						# min and abs in order to treat convergent and divergent cones
						lEle = self.shortestLbd[idx] / dofPerLbd
						# lEle = phys.c / fMax / dofPerLbd  # value before (cylinder)

					beta = lgt / lEle
					if beta < supLim:  # one element of order ceil(beta)
						nbEles = 1
						self.rs.append(int(np.max([infLim, np.ceil(beta)])))

					elif beta >= supLim:
						nbEles = np.ceil(beta / supLim)  # we will have nbEles elements
						# TODO : rs is a nonlinear function of beta, ie, greater than beta, see photo and recording of our voices 13/12/18
						self.rs.extend([int(np.max([infLim, np.ceil(beta / nbEles)]))] * int(nbEles))  # of order beta / gamma
						# lEleAdapt = lgt / nbEles
					else:
						raise ValueError("cannot have negative value for element length!")
					xElesBordPart = np.linspace(x[0], x[1], nbEles + 1)
					self.mesh.extend(xElesBordPart[0: -1])
					# et là je dois caler un self.lElesOrdre qui se rempli à la case du
					# bon ordre avec les longueurs des éléments qui viennent d'être créés
					# pdb.set_trace()
					self.lElesOrder[self.rs[-1] - 1] = np.concatenate(
						(self.lElesOrder[self.rs[-1] - 1], np.diff(xElesBordPart)))

					xLims = np.array([xElesBordPart[0: -1], xElesBordPart[1:]]).transpose()
					if part[0] == "linear":
						rElesPart = (r[0] + (xLims - x[0]) * (r[1] - r[0]) / (x[1] -
						                                                      x[0])).tolist()
					elif part[0] == "bessel":
						d = part[1]
						gamma = part[4]
						b = (d / (r[0] ** (-1 / gamma) - r[1] ** (-1 / gamma))) ** gamma
						x0 = d * r[1] ** (-1 / gamma) / (r[0] ** (-1 / gamma) - r[1] ** (-1 /
						                                                                 gamma))
						xChVar = part[2][1] + x0 - xLims
						rElesPart = (b * xChVar ** (- gamma)).tolist()
					elif part[0] == "exponential":
						pass
					elesPart = list(zip([part[0]] * len(xLims.tolist()), xLims.tolist(),
					                    rElesPart, [part[4]] * len(xLims.tolist())))
					self.eles.extend(elesPart)
				self.rs = np.array(self.rs)

			else:
				lEle = param[1]
				for part in instru.parts:
					x = part[2]
					r = part[3]
					nbEleBorderPart = int(max(np.floor((x[1] - x[0]) / lEle) + 1, 2))
					xElesBordPart = np.linspace(x[0], x[1], nbEleBorderPart)
					self.mesh.extend(xElesBordPart[0: -1])
					xLims = np.array([xElesBordPart[0: -1], xElesBordPart[1:]]).transpose()
					if part[0] == "linear":
						rElesPart = (r[0] + (xLims - x[0]) * (r[1] - r[0]) / (x[1] -
						                                                      x[0])).tolist()
					elif part[0] == "bessel":
						d = part[1]
						gamma = part[4]
						b = (d / (r[0] ** (-1 / gamma) - r[1] ** (-1 / gamma))) ** gamma
						x0 = d * r[1] ** (-1 / gamma) / (r[0] ** (-1 / gamma) - r[1] ** (-1 /
						                                                                 gamma))
						xChVar = part[2][1] + x0 - xLims
						rElesPart = (b * xChVar ** (- gamma)).tolist()
					elif part[0] == "exponential":
						pass
					elesPart = list(zip([part[0]] * len(xLims.tolist()), xLims.tolist(),
					                    rElesPart, [part[4]] * len(xLims.tolist())))
					self.eles.extend(elesPart)
			self.mesh.append(instru.ltot)
			self.lEles = np.diff(self.mesh)
			self.nbEles = len(self.mesh) - 1

	def draw(self, symmetric="yes"):
		"""Draw only the mesh without the nodes for the formula."""
		x2plot = [ele[1] for ele in self.eles]
		r2plot = [ele[2] for ele in self.eles]
		toplot = sum(zip(x2plot, r2plot), ())
		plt.plot(*toplot)
		if symmetric == "yes":
			r2plotSym = [[- rs[0], - rs[1]] for rs in r2plot]
			toplotsym = sum(zip(x2plot, r2plotSym), ())
			plt.plot(*toplotsym)
			plt.plot([x2plot[0][0], x2plot[-1][1]], np.zeros(shape=(2,)),
			         '--k', linewidth=0.5)
			plt.show()

	def save(self, dataPath, method):
		"""Save the mesh.
		"""
		if method == "json":
			with open(dataPath + "/mesh.json", "w") as fp:
				json.dump({"eles": self.eles, "mesh": self.mesh, "lEles":
				           self.lEles.tolist(), "nbEles": self.nbEles}, fp)
		elif method == "pickle":
			with open(dataPath + "/mesh.p", "wb") as fp:
				myPck = pickle.Pickler(fp)
				myPck.dump(self)
		else:
			raise ValueError("the saving method you ask for is not understood")


class Nodes:
	"""calculate the nodes for the quadrature for the given instrument."""

	def __init__(self, *param):
		"""Create the node over the mesh according to a quadrature method.

		The method used here is the GaussLobatoQuadrature.
		"""
		if isinstance(param[0], str):
			if param[0][-2:] == ".p":
				with open(param[0], 'rb') as fp:
					dataPickle = pickle.Unpickler(fp)
					data2copy = dataPickle.load()
					self.pts = data2copy.pts
					self.weight = data2copy.weight
					self.BK = data2copy.BK
					self.xNodes = data2copy.xNodes
					self.rLoc = data2copy.rLoc
					self.sLoc = data2copy.sLoc
			elif param[0][-5:] == ".json":
				with open(param[0], "r") as fp:
					nodes = json.load(fp)
					self.pts = np.array(nodes["pts"])
					self.weight = np.array(nodes["weight"])
					self.BK = np.array(nodes["BK"])
					self.xNodes = np.array(nodes["xNodes"])
					self.rLoc = np.array(nodes["rLoc"])
					self.sLoc = np.array(nodes["sLoc"])
		elif len(param) < 2:  # adaptative mesh
			mesh = param[0]
			self.lElesOrder = mesh.lElesOrder
			self.rs = mesh.rs
			self.quads = []
			for r in range(mesh.rs.max()):  # computing every quadrature for any order
				self.quads.append(GLQuad(r + 1))
			self.xNodes = [np.array([])] * mesh.rs.max()
			self.rLoc = [np.array([])] * mesh.rs.max()
			self.sLoc = [np.array([])] * mesh.rs.max()
			self.idxOrder = []
			self.xNodesLin = []
			for idx, ele in enumerate(mesh.eles):
				curR = mesh.rs[idx]
				xLim = ele[1]
				rLim = ele[2]
				# the idea is to fill stacks of elements of the same order for xNodes
				# and rLoc, to compute as blocks of element of the same order for losses
				# pdb.set_trace()
				# to get a co vect adding a dimension
				rowXNodes = mesh.mesh[idx] + self.quads[curR - 1].pts * mesh.lEles[idx]
				# pdb.set_trace()
				self.xNodes[curR - 1] = \
					np.hstack((self.xNodes[curR - 1], rowXNodes[:, None])) if \
					self.xNodes[curR - 1].size else rowXNodes[:, None]
				self.idxOrder.append(self.xNodes[curR - 1].shape[1] - 1)
				self.xNodesLin.extend(rowXNodes)  # you can extend with np.array lists.
				if ele[0] == "linear":
					# pdb.set_trace()
					rowRLoc = rLim[0] + (self.xNodes[curR - 1][:, -1] - xLim[0]) * \
						(rLim[1] - rLim[0]) / (xLim[1] - xLim[0])
				elif ele[0] == "bessel":
					d = xLim[1] - xLim[0]
					gamma = ele[3]
					b = (d / (rLim[0] ** (-1 / gamma) - rLim[1] ** (-1 / gamma))) ** gamma
					x0 = d * rLim[1] ** (-1 / gamma) / (rLim[0] ** (-1 / gamma) -
					                                    rLim[1] ** (-1 / gamma))
					xChVar = xLim[1] + x0 - self.xNodes[curR - 1][:, -1]
					rowRLoc = (b * xChVar ** (- gamma)).tolist()
				self.rLoc[curR - 1] = \
					np.hstack((self.rLoc[curR - 1], rowRLoc[:, None])) if \
					self.rLoc[curR - 1].size else rowRLoc[:, None]
				# pdb.set_trace()
				rowSLoc = np.pi * self.rLoc[curR - 1][:, -1] ** 2
				self.sLoc[curR - 1] = \
					np.hstack((self.sLoc[curR - 1], rowSLoc[:, None])) if \
					self.sLoc[curR - 1].size else rowSLoc[:, None]
			self.idxOrder = np.array(self.idxOrder)
		else:
			mesh = param[0]
			r = param[1]
			self.r = r
			# There is a new class because the calulation method of the
			# integral could be different
			gLQuad = GLQuad(r)
			self.pts = gLQuad.pts
			self.weight = gLQuad.weight
			self.BK = gLQuad.BK
			self.xNodes = mesh.mesh[: -1] + np.outer(self.pts, mesh.lEles)
			self.xNodesLin = self.xNodes.flatten('F')
			self.rLoc = np.empty(self.xNodes.shape)
			for idx, ele in enumerate(mesh.eles):
				xLim = ele[1]
				rLim = ele[2]
				if ele[0] == "linear":
					self.rLoc[:, idx] = rLim[0] + (self.xNodes[:, idx] - xLim[0]) * \
						(rLim[1] - rLim[0]) / (xLim[1] - xLim[0])
				elif ele[0] == "bessel":
					d = xLim[1] - xLim[0]
					gamma = ele[3]
					b = (d / (rLim[0] ** (-1 / gamma) - rLim[1] ** (-1 / gamma))) ** gamma
					x0 = d * rLim[1] ** (-1 / gamma) / (rLim[0] ** (-1 / gamma) -
					                                    rLim[1] ** (-1 / gamma))
					xChVar = xLim[1] + x0 - self.xNodes[:, idx]
					self.rLoc[:, idx] = (b * xChVar ** (- gamma)).tolist()
			self.sLoc = np.pi * self.rLoc ** 2

	def draw(self, symmetric="yes", options=dict()):
		"""Draw the discretized bore."""
		try:
			params = []
			if isinstance(self.xNodes, list):  # adaptative mesh
				# we create each curve order mise (not following the element order along
				# the bore axis)
				for order in range(len(self.xNodes)):  # order is order - 1 (array idx)
					if len(self.xNodes[order]) == 0:
						continue
					xEles = self.xNodes[order]
					rEles = self.rLoc[order]
					# pdb.set_trace()
					for idx in range(xEles.shape[1]):
						# pdb.set_trace()
						x2Plot = xEles[:, idx].transpose().tolist()
						r2Plot = rEles[:, idx].transpose().tolist()
						rNeg2Plot = -rEles[:, idx]
						rNeg2Plot = rNeg2Plot.transpose().tolist()
						param = {}
						xEle = x2Plot
						rEle = r2Plot
						data = {'x': xEle, 'y': rEle}
						param.update(data)
						param.update(options)
						params.append(go.Scatter(param))
						param = {}
						rEle = rNeg2Plot
						data = {'x': xEle, 'y': rEle}
						param.update(data)
						param.update(options)
						params.append(go.Scatter(param))
			else:
				x2Plot = self.xNodes.transpose().tolist()
				r2Plot = self.rLoc.transpose().tolist()
				rNeg2Plot = -self.rLoc
				rNeg2Plot = rNeg2Plot.transpose().tolist()
				for idx in range(0,len(x2Plot)):
					param = {}
					xEle = x2Plot[idx]
					rEle = r2Plot[idx]
					data = {'x': xEle, 'y': rEle}
					param.update(data)
					param.update(options)
					params.append(go.Scatter(param))
					param = {}
					rEle = rNeg2Plot[idx]
					data = {'x': xEle, 'y': rEle}
					param.update(data)
					param.update(options)
					params.append(go.Scatter(param))
			return params
		except:
			if isinstance(self.xNodes, list):  # adaptative mesh
				raise ValueError("cannot draw adaptative nodes using matplotlib," +
				                 "use plotly instead")
			if len(options) > 0:
				symmetric = options
			else:
				symmetric = "yes"
			plt.plot(self.xNodes, self.rLoc, 'o-')
			if symmetric == "yes":
				plt.plot(self.xNodes, -self.rLoc, 'o-')
				plt.plot(self.xNodes.transpose().flatten()[[0, -1]], np.zeros(2,),
				         '--k', linewidth=0.5)
			plt.show()

	def save(self, dataPath, method):
		"""Save the discretized geometry.

		You can chose either pickle or json format!!!!
		"""
		if method == "json":
			with open(dataPath + "/nodes.json", "w") as fp:
				json.dump({"pts": self.pts.tolist(), "weight": self.weight.tolist(),
				           "BK": self.BK.tolist(), "xNodes": self.xNodes.tolist(),
				           "rLoc": self.rLoc.tolist(), "sLoc": self.sLoc.tolist()}, fp)
		elif method == "pickle":
			with open(dataPath + "/nodes.p", "wb") as fp:
				myPck = pickle.Pickler(fp)
				myPck.dump(self)
		else:
			raise ValueError("the saving method you ask for is not understood")


# Discretisation class test
if __name__ == "__main__":
	coneconeGeom = [["linear", 3e-1, [1e-1, 2e-1]],
	                ["linear", [3e-1, 4e-1], [9e-2, 2e-1], []],
	                [[4e-1, 5e-1], [3e-1, 1e-1]],
	                ["linear", 2e-2, [9e-2, 2e-1], []],
	                [1.5e-1, [2e-1, 1e-1]],
	                ["bessel", 1.1e-1, [1e-1, 2e-1], 0.7]]
	conecone = Instrument(coneconeGeom)
	# cylcone.draw()
	# plt.show()
	print('coucou')
	mesh = Mesh(conecone, 5e-2, {"type": "adaptative"})
	mesh.draw()
	mesh.save("./Workspace", "json")
	mesh.save("./Workspace", "pickle")
	mm = Mesh("Workspace/mesh.p")
	mm.draw()
	pp = Mesh("Workspace/mesh.json")
	pp.draw()
	nodes = Nodes(pp, 6)
	plt.figure()
	nodes.draw()
	nodes.save("./Workspace", "json")
	nodes.save("./Workspace", "pickle")
	del nodes
	qq = Nodes("Workspace/nodes.p")
	qq.draw()
	del qq
	rr = Nodes("Workspace/nodes.json")
	aa=rr.draw()
	#del rr
	print("coucou")
