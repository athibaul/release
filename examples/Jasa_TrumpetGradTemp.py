
# you should change your pythonpath, but this trick work
import sys
sys.path.append('../')

from openwind.Instrument import Instrument
from openwind.Discretisation import Mesh, Nodes
from openwind.Physics import Physics
from openwind.Impedance import Impedance
import numpy as np

__author__ = "Juliette Chabassier, Robin Tournemenne"
__copyright__ = "Copyright 2019, Inria"
__credits__ = ["Juliette Chabassier", "Robin Tournemenne"]
__license__ = "GPL 3.0"
__version__ = "1.0"
__maintainer__ = "Juliette Chabassier"
__email__ = "juliette.chabassier@inria.fr"
__status__ = "Dev"

damping = True #  False # True
adim = False
F1 = 20
F2 = 1200
fs =  np.arange(F1, F2, 1)
print('param loaded')

instru = Instrument('Tr_co_MP.txt')
N = 72
def GradT(x):
	return 37 - (37 - 21) * x / instru.ltot

temp = np.mean(GradT(np.linspace(0, instru.ltot, 100)))
print('temperature ' + str(temp))

print("nombre d'elements requis: " + str(N))

mesh = Mesh(instru, instru.ltot / N)

print("nombre d'elements produits: " + str(mesh.nbEles))

nodes = Nodes(mesh, ordre)
physics = Physics(temp)

## ------------------------------------------------------------------------
zFEMCst = Impedance(fs, instru, mesh, nodes, physics,
                    {'damping': damping, 'adim': adim})
curves = [zFEMCst.draw()]

physicsNonConstant = Physics(GradT)
zFEMGrad = Impedance(fs, instru, mesh, nodes, physicsNonConstant,
                     {'damping': damping, 'adim': adim})
curves = [zFEMGrad.draw()]

print('----------ordre: ' + str(ordre))
zFEM = Impedance(fs, instru, mesh, nodes, physics,
                 {'damping': damping, 'adim': adim})
curves.append(zFEM.draw())
