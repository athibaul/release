
# you should change your pythonpath, but this trick work
import sys
sys.path.append('../')

from openwind.Instrument import Instrument
from openwind.Discretisation import Mesh, Nodes
from openwind.Physics import Physics
from openwind.Impedance import Impedance
import numpy as np

__author__ = "Juliette Chabassier, Robin Tournemenne"
__copyright__ = "Copyright 2019, Inria"
__credits__ = ["Juliette Chabassier", "Robin Tournemenne"]
__license__ = "GPL 3.0"
__version__ = "1.0"
__maintainer__ = "Juliette Chabassier"
__email__ = "juliette.chabassier@inria.fr"
__status__ = "Dev"

damping = True
F1 = 20
F2 = 2000
fs = np.arange(F1, F2, 0.1)
temp = 25
subdivisions = [1, 6, 16, 30, 50]
print('param loaded')
##
physics = Physics(temp)
param = [1, 1e-3, 1]
lgt = param[0]
rd = param[1]
alpha = param[2]
x = [0, lgt]
r = [rd, rd + lgt * np.tan(alpha * np.pi / 180)]
instru = Instrument(x, r)

N = 12
ordre = 11
print("nombre d'elements requis: " + str(N))

mesh = Mesh(instru, instru.ltot / N)

print("nombre d'elements produits: " + str(mesh.nbEles))

nodes = Nodes(mesh, ordre)

print('----------ordre: ' + str(ordre))
zFEM = Impedance(fs, instru, mesh, nodes, physics, {'damping': damping})
curves = [zFEM.draw()]

TES = np.empty(len(subdivisions))
ErrorTMM = np.empty(len(subdivisions))
zTMM = []
for i, subdiv in enumerate(subdivisions):
	print(subdiv)
	zTMM.append(Impedance(fs, instru, physics, {'damping': damping,
																							'nbSub': subdiv}))
	curves.append(zTMM[-1].draw())
	TES[i] = instru.ltot / subdiv
	ErrorTMM[i] = Impedance.l2error(zTMM[-1], zFEM)
