
# you should change your pythonpath, but this trick work
import sys
sys.path.append('../')

from openwind.Instrument import Instrument
from openwind.Discretisation import Mesh, Nodes
from openwind.Physics import Physics
from openwind.Impedance import Impedance
import numpy as np
from timeit import timeit
import cProfile

__author__ = "Juliette Chabassier, Robin Tournemenne"
__copyright__ = "Copyright 2019, Inria"
__credits__ = ["Juliette Chabassier", "Robin Tournemenne"]
__license__ = "GPL 3.0"
__version__ = "1.0"
__maintainer__ = "Juliette Chabassier"
__email__ = "juliette.chabassier@inria.fr"
__status__ = "Dev"

damping = True
F1 = 20
F2 = 2000
fs = np.arange(F1, F2, 1)
temp = 25
subdivisions = np.hstack((np.arange(1, 20, 1), np.array([22, 26, 32, 40, 56, 82,
                                                         144])))

print('param loaded')
physics = Physics(temp)

# Trompette
instru = Instrument('Tr_co_MP.txt')

lbdmin = physics.c / F2
# if 'zFEMCvg' not in globals():
lEleCvg = lbdmin / 10
orderCvg = 14
meshCvg = Mesh(instru, lEleCvg)
nodesCvg = Nodes(meshCvg, orderCvg)

print('-------computation FEM converged')
zFEMCvg = Impedance(fs, instru, meshCvg, nodesCvg, physics, {'damping':
                                                             damping})

lEleClassic = lbdmin / 2
# lossless: 11 pour lbdmin / 2 est le mieux! (385 dof, err ~= 3e-12)
ordreClassic = 4  # 4 is ok for Lossy
dofPerLbd = 10

#I take advantage of the porte des variables in python... ugly but working
mesh = Mesh(instru, lEleClassic)

meshAdapt = Mesh(instru, dofPerLbd, physics, F2, {'infLim': 1, 'supLim': 11})
ordreSpec = 3
meshAdapt.rs[1] = ordreSpec
meshAdapt.rs[2] = ordreSpec
meshAdapt.rs[3] = ordreSpec
meshAdapt.rs[4] = ordreSpec - 1
meshAdapt.rs[5] = ordreSpec * 2 - 1
meshAdapt.rs[10:12] = ordreSpec * 2
meshAdapt.rs[12] = ordreSpec * 2 - 1
meshAdapt.rs[-1] = ordreSpec
meshAdapt.rs[-11] = ordreSpec
meshAdapt.lElesOrder = [np.array([])] * np.max(meshAdapt.rs)
for idx, curR in enumerate(meshAdapt.rs):
	meshAdapt.lElesOrder[curR - 1] = np.hstack(
		(meshAdapt.lElesOrder[curR - 1], meshAdapt.lEles[idx]))

nodes = Nodes(mesh, ordreClassic)
print('--------- computation FEM')
zFEM = Impedance(fs, instru, mesh, nodes, physics, {'damping': damping})
nodesAdapt = Nodes(meshAdapt)
print('--------- computation FEM adapt')
zFEMAdapt = Impedance(fs,
                      instru, meshAdapt, nodesAdapt, physics, {'damping':
                                                                   damping})
errorFEM = Impedance.l2error(zFEM, zFEMCvg)
errorFEMAdapt = Impedance.l2error(zFEMAdapt, zFEMCvg)
zTMM = []
errorsTMM = []
for subdiv in subdivisions:
	print('-------- computation TMM ' + str(subdiv))
	zTMM.append(Impedance(fs, instru, physics, {'damping': damping,
	                                            'nbSub': subdiv}))
	errorsTMM.append(Impedance.l2error(zTMM[-1], zFEMCvg))

##
def FEM():
	nodes = Nodes(mesh, ordreClassic)
	# calculs d'impédance
	zFEM = Impedance(fs, instru, mesh, nodes, physics, {'damping': damping})

def FEMAdapt():
	nodesAdapt = Nodes(meshAdapt)
	zFEMAdapt = Impedance(fs, instru, meshAdapt, nodesAdapt, physics, {'damping':
	                                                                   damping})

print('CPUFEM')
repe = 5
CPUFEM = timeit(FEM, number=repe)
cProfile.run('FEM()', 'ProfFEM')
print(CPUFEM / repe)
print('CPUAdapt')
CPUFEMAdapt = timeit(FEMAdapt, number=repe)
cProfile.run('FEMAdapt()', 'ProfFEMAdapt')
print(CPUFEMAdapt / repe)
print(str((1 - CPUFEMAdapt / CPUFEM) * 100) + '%')
CPUFEM = CPUFEM / repe
CPUFEMAdapt = CPUFEMAdapt / repe
print('--------------------')

def wrapper(subdiv):
	def TMM():
		zTMM = Impedance(fs, instru, physics, {'damping': damping,
		                                       'nbSub': subdiv})
	return TMM


CPUTMM = []
for subdiv in subdivisions:
	TMMGoodVal = wrapper(subdiv)
	CPUTMM.append(timeit(TMMGoodVal, number=repe) / repe)
