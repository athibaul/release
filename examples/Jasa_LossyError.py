
# you should change your pythonpath, but this trick work
import sys
sys.path.append('../')

from openwind.Instrument import Instrument
from openwind.Discretisation import Mesh, Nodes
from openwind.Physics import Physics
from openwind.Impedance import Impedance
import numpy as np

__author__ = "Juliette Chabassier, Robin Tournemenne"
__copyright__ = "Copyright 2019, Inria"
__credits__ = ["Juliette Chabassier", "Robin Tournemenne"]
__license__ = "GPL 3.0"
__version__ = "1.0"
__maintainer__ = "Juliette Chabassier"
__email__ = "juliette.chabassier@inria.fr"
__status__ = "Dev"

damping = True
ordres = [10, 11]
temp = 25
params = [[1e-2, 1e-3, 45],  # cuvette d'embouchure
          [1e-1, 1e-3, 10],  # queue d'embouchure
          [1e-1, 1e-2, 1],  # leadpipe pieces
          [1, 1e-3, 1],  # conical instrument 1
          [1, 1e-2, 1],  # conical instrument 2
          5000,  # double cône discontinus
          1000,  # trompette
]

names = ["Cup", "Backbore", "Leadpipe", "ConicalIns1", "ConicalIns2",
         "discont", "Trumpet"]
subdivisions = np.hstack((np.arange(1, 20, 1), np.array([22, 26, 32, 40, 56, 82, 144])))
Ns = [24, 24, 24, 60, 60, 24, 72]
F1 = 20
F2 = 2000
fs = np.arange(F1, F2, 1)
print('param loaded')
physics = Physics(temp)

for idx, param in enumerate(params):
	if idx != 1:
		continue
	print(names[idx])
	if param == 1000:
		xr = np.loadtxt('Tr_co_MP.txt')
		x = xr[:, 0].tolist()
		r = xr[:, 1].tolist()
		instru = Instrument('Tr_co_MP.txt')
	elif param == 5000:
		x = [0, 5e-2, 5e-2, 1e-1]
		r = [1e-2, 2e-2, 2.5e-2, 2.2e-2]
		instru = Instrument(x, r)
	else:
		lgt = param[0]
		rd = param[1]
		alpha = param[2]
		x = [0, lgt]
		r = [rd, rd + lgt * np.tan(alpha * np.pi / 180)]
		if alpha == 45 and lgt == 1e-2 and rd == 1e-3:
			rinit = r[1]
			r[1] = r[0]
			r[0] = rinit
		instru = Instrument(x, r)

	N = Ns[idx]
	print("nombre d'elements requis: " + str(N))

	mesh = Mesh(instru, instru.ltot / N)

	print("nombre d'elements produits: " + str(mesh.nbEles))

	zFEM = []
	for r in ordres:
		nodes = Nodes(mesh, r)
		print('----------ordre: ' + str(r))
		zFEM.append(Impedance(fs, instru, mesh, nodes, physics, {'damping': damping}))

	TES = np.empty(len(subdivisions))
	eps = np.empty(len(subdivisions))
	ErrorTMM = np.empty(len(subdivisions))
	ErrorFEM = Impedance.l2error(zFEM[0], zFEM[1])
	print("converged if value inf to 1e-12: " + str(ErrorFEM))
	zTMM = []
	curves = []
	for i, subdiv in enumerate(subdivisions):
		print(subdiv)
		zTMM.append(Impedance(fs, instru, physics, {'damping': damping,
																								'nbSub': subdiv}))
		curves.append(zTMM[-1].draw())
		if param == 1000:
			TES[i] = 0.002 / subdiv
		elif param == 5000:
			TES[i] = 5e-2 / subdiv
		else:
			TES[i] = param[0] / subdiv
		ErrorTMM[i] = Impedance.l2error(zTMM[-1], zFEM[1])
