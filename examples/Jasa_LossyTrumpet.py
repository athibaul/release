
# you should change your pythonpath, but this trick work
import sys
sys.path.append('../')

from openwind.Instrument import Instrument
from openwind.Discretisation import Mesh, Nodes
from openwind.Physics import Physics
from openwind.Impedance import Impedance
import numpy as np

__author__ = "Juliette Chabassier, Robin Tournemenne"
__copyright__ = "Copyright 2019, Inria"
__credits__ = ["Juliette Chabassier", "Robin Tournemenne"]
__license__ = "GPL 3.0"
__version__ = "1.0"
__maintainer__ = "Juliette Chabassier"
__email__ = "juliette.chabassier@inria.fr"
__status__ = "Dev"

damping = True
F1 = 20
F2 = 2000
ordres = np.arange(1, 13, 1)
fs = np.arange(F1, F2, 1)
temp = 25
print('param loaded')

instru = Instrument('Tr_co_MP.txt')
physics = Physics(temp)

lbd = physics.c / F2
N = np.ceil(10 * instru.ltot / lbd)
print("nombre d'elements requis: " + str(N))


mesh = Mesh(instru, instru.ltot / N)

print("nombre d'elements produits: " + str(mesh.nbEles))

curves = []
zFEM = []
Error = np.empty(len(ordres) - 1)
for r in ordres:
	nodes = Nodes(mesh, r)
	print('----------ordre: ' + str(r))
	zFEM.append(Impedance(fs, instru, mesh, nodes, physics, {'damping': damping}))
	curves.append(zFEM[-1].draw())
	if r != 1:
		Error[r - 2] = Impedance.l2error(zFEM[-1], zFEM[-2])
