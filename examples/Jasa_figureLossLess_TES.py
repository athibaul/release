
# you should change your pythonpath, but this trick work
import sys
sys.path.append('../')

from openwind.Instrument import Instrument
from openwind.Discretisation import Mesh, Nodes
from openwind.Physics import Physics
from openwind.Impedance import Impedance
import numpy as np

__author__ = "Juliette Chabassier, Robin Tournemenne"
__copyright__ = "Copyright 2019, Inria"
__credits__ = ["Juliette Chabassier", "Robin Tournemenne"]
__license__ = "GPL 3.0"
__version__ = "1.0"
__maintainer__ = "Juliette Chabassier"
__email__ = "juliette.chabassier@inria.fr"
__status__ = "Dev"

damping = False
F1 = 20
F2 = 2000
ordres = np.arange(1, 7, 1)
TESs = [1e-1, 5e-2, 3e-2, 2e-2, 1e-2, 5e-3, 3e-3, 2e-3, 1e-3]
fs = np.arange(F1, F2, 1)
temp = 25
print('param loaded')
instru = Instrument('Tr_co_MP.txt')
physics = Physics(temp)

# we can place a fourth argument containing parameters (that will be usefull for
# the error estimation of the TMM under lossy propagation
zTMM = Impedance(fs, instru, physics)
curves = [zTMM.draw()]

zFEM = []
Error = np.empty((len(TESs), len(ordres)))
for i, TES in enumerate(TESs):
	print("TES: " + str(TES))
	mesh = Mesh(instru, TES)

	print("nombre d'elements produits: " + str(mesh.nbEles))
	for j, r in enumerate(ordres):

		nodes = Nodes(mesh, r)
		print('----------ordre: ' + str(r))
		zFEM.append(Impedance(fs, instru, mesh, nodes, physics, {'damping': damping}))
		curves.append(zFEM[-1].draw())
		Error[i, j] = Impedance.l2error(zFEM[-1], zTMM)
